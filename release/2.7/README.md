## 2.7.0 Что нового:

#### Доработки в GitFlic

 + Добавили функцию входа при помощи LDAP (доступно для self-hosted версии)
 + Добавили создание собственных лейблов в проекте
 + Добавили редактирование ролей у участников компаний
 + Добавили кэш на страницу веток
 + Добавили пагинацию на страницу веток
 + Добавили теги для агентов (доступно для self-hosted версии)

#### Исправления в GitFlic

 + Исправили ошибки с определением ролей пользователей, при работе с правилами запросов на слияние
 + Исправили верстку на странице проектов
 + Исправили формулировки на странице панели управления (доступно в self-hosted версии)
 + Исправили сортировку тегов

---

## 2.7.1 Что нового:

#### Доработки в GitFlic

+ Добавили страницу с пользователями, кто добавил проект в избранное
+ Добавили страницу с пользователями, кто подписался на проект

#### Исправления в GitFlic

 + Исправили ошибки с редактированием прав участников компании
 + Исправили сортировку тегов в проекте
 + Исправили заголовок страницы 404-й ошибки
 + Исправили кеш веток
 + Небольшое исправление верстки

 ---

## 2.7.2 Что нового:

#### Доработки в GitFlic

+ Добавили дополнительные настройки в панели администратора (доступно в self-hosted версии)
+ Добавили логирование событий пользователей для действий pull/push (доступно в self-hosted версии)
+ Добавили срок жизни для уведомлений на странице уведомлений 

#### Исправления в GitFlic

+ Исправили верстку на странице с подписчиками профиля
+ Исправили размер шрифта в комментариях и дисскуссиях (проблема из 2.7.0)
+ Исправили ссылки на переход к просмотру статистики избранного и наблюдателей в проекте