### 2.4.0 Что нового:

#### Масштабная переработка образовательной части GitFlic

+ Переработан внешний вид и логика взаимодействия пользователя с сервисом

#### Исправления в основной части GitFlic

+ Исправили отображение времени последнего обновления проекта
+ [Исправили](https://gitflic.ru/project/gitflic/gitflic/issue/258) ошибку отображения количества комментариев внутри проблемы
+ Исправили заливку на странице со списком запросов на слияние