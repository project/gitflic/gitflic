### 2.3.3 Что нового:

+ [Добавили](https://gitflic.ru/project/gitflic/gitflic/issue/89) в фильтр проблем "Кто создал" всех авторов проблем
+ Добавили ярлык на запрос на слияние, если запрос назначен на вас
+ Добавили отправку комментариев по сочетанию клавиш
+ Исправили ошибку скачавания файлов и архива с названием кириллицой
+ [Исправили](https://gitflic.ru/project/gitflic/gitflic/issue/251) выравнивание столбцов в списке файлов проекта
+ Исправили ошибку при удалении проекта с книгой
+ Исправили верстку таблиц в markdown
+ Исправили опечатку в уведомлениях Telegram
+ [Исправили](https://gitflic.ru/project/gitflic/gitflic/issue/254) переход к начальной странице книги
+ Технические доработки


### 2.3.1 Что нового:

+ Технические доработки

### 2.3.0 Что нового:

##### Новый функционал:

+ Книги проектов. Git-book - инструмент для создания централизованной документации к вашему проекту.
+ Уведомления в Telegram. Настройте получение уведомлений в групповые чаты или паблики, чтобы усилить вовлеченность команды в проект.

##### Исправление и доработки:

+ Доработали окно комментирования кода в запросе на слияние
+ Добавили тултипы для некоторых кнопок
+ Добавили дополнительные настройки для правил к запросам на слияние
+ Добавили фильтр на странице изменений в запросе на слияние. Теперь можно отделить файлы по ЯП и смотреть сначала все файлы одного языка, а затем другого.
+ [Добавили](https://gitflic.ru/project/gitflic/gitflic/issue/44) возможность загрузить проект архивом .zip
+ Исправили ошибку, возникающую при создании компании со страницы команды
+ Исправили ошибку подсчета людей в команде
+ [Исправили](https://gitflic.ru/project/gitflic/gitflic/issue/243) ошибку добавления проекта в избранное со страницы просмотра файла в проекте
+ [Исправили](https://gitflic.ru/project/gitflic/gitflic/issue/230) размер заголовков в markdown файлах
+ Исправили верстку на странице оплаты
+ Исправили ошибку с поиском языка C на странице создания форка
+ [Исправили](https://gitflic.ru/project/gitflic/gitflic/issue/249) размер колонки с названием коммита в списке файлов проекта