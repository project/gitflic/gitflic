## 3.2.1 Что нового:

Обязательно прочитайте инструкцию по обновлению с версии 2.х.х до версии 3.х.х по [ссылке](https://docs.gitflic.space/setup/update_to_3.x.x).

Для корректной работы CI/CD не забудьте обновить [GitFlic Runner](https://gitflic.ru/project/gitflic/runner/release/latest)

### Добавили:

* Выбор шаблонных конфигураций для CI/CD при создании/настройке проекта
* Поддержку реестра пакетов R
* Поддержку OIDC SSO (Self-Hosted)
* Блок с кодом для клонирования проекта на странице с деревом файлов
* API метод для поиска пользователей по почте или логину (Self-Hosted)
* Параметры для поиска проектов по названию к API методам

### Исправили:

* Валидацию при создании ветки в интерфейсе загрузки файлов
* Верстку 2FA в настройках профиля
* Верстку дополнительной авторизации в настройках профиля
* Верстку на странице настраиваемых ролей
* Верстку области действия кнопки 'Начать новую дискуссию'
* Верстку отображения переменных CI/CD
* Верстку подсказки в редакторе создания проблемы на мобильной версии
* Верстку поп-апа настройки одобрений запросов на слияние
* Верстку таблиц в markdown
* Верстку у таблиц с агентами в настройках компании
* Вывод логов в утилите CLI
* Вывод сообщения об ошибке при одобрении ЗнС
* Генерацию ссылки на проект в readme-проекта
* Дублирование ошибки при создании пользователя в панели администратора
* Загрузку файлов в пустой репозиторий компании
* Задвоение ошибки при создании правила защиты ветки с пустым полем
* Неточности в уведомлениях по событиям в сервисе
* Отображение кнопки дискуссий в ЗнС
* Отображение подсказок на странице настроек ЗнС
* Отображение пользователей на странице статистики проекта по вкладу коммитов
* Отображение проекта при наличии директории .gitmodules
* Отображение сабмодуля в проекте, когда он не в корневой директории
* Отображение статуса на коммитах, подписанных GPG-ключом
* Отображение статусов конвейеров с триггерными задачами
* Ошибки в работе пользовательских ролей
* Ошибку в работе реестра PyPi
* Ошибку при добавлении топика проекта
* Ошибку при работе с реестром по REST-API
* Ошибку при удалении агента администратором
* Ошибку создания дублирующих ЗнС
* Переводы и описания полей
* Переводы статусов в вебхуках
* Переводы squash-коммитов в настройках
* Плейсхолдер для конфигурации CI/CD
* Работу редактора на странице релизов
* Работу сортировки на странице тегов и релизов
* Работу вебхука по событию нового комментария в проблеме
* Работу API-методов для компаний
* Размер пагинации на странице реестра пакетов
* Расположение кнопки удаления файлов на дереве файлов
* Содержание ответов в API методах ЗнС
* Содержание ответов в API методах проектов пользователя
* Сообщение при передаче проектов, команд и компаний
* Сортировку стадий конвейера на странице ЗнС
* Стиль отображения чекбоксов в markdown
* Уведомление при смене данных пользователей LDAP
* Уведомление с ошибкой при парсинге yaml файла
* Управление настройкой автослияния веток в командах/компаниях
* Цвет уведомления при создании API-токена
* API-метод для защиты веток проекта

---

## 3.2.0 Что нового:

### Добавили:

+ Редактор кода
+ Загрузку файлов из интерфейса
+ Реестр пакетов OneScript
+ RBAC (расширенная ролевая модель) (CloudWeb, Self-Hosted Enterprise)
+ Интеграцию с SCA анализаторами (Self-Hosted Enterprise)
+ Интеграцию с Jira
+ Поддержку GO-модулей
+ Отображение результатов UNIT-тестов (Self-Hosted OnPremise, Self-Hosted Enterprise)
+ Аналитику по вкладу коммитеров в проекте
+ Механизм наследования настроек шаблонов защищенных тегов/веток/одобрений из компаний/команд в запросах на слияние
+ Возможность указать стартовую страницу для книги проекта
+ Расширили возможность настройки вебхуков
+ Кнопку выхода из проектов для участников
+ Массовую загрузку библиотек в реестр пакетов PyPi
+ Отображение количества изменённых строк в ЗнС
+ Поддержку сносок в markdown-обработчик
+ Поддержку Readme.txt
+ Создание проекта с файлом readme и .gitignore
+ Сортировку Тегов и Релизов
+ Фильтры на странице уведомлений
+ Фильтры на страницы тегов и релизов
+ Форматы .apk/.aab в доступные для файлов релиза
+ Typst в список языков программирования
+ Возможность указать в include:remote ссылку на файл (CI/CD)
+ Ключевые слова before/after-script для конвейеров (CI/CD)
+ Запрет на использование старого пароля при его смене пользователем (Self-Hosted)
+ Запрет на смену данных пользователя, полученных через LDAP (Self-Hosted)


### Исправили:

+ Валидацию на пробелы в имени пользователя
+ Верстку пагинатора на страницах сервиса
+ Возможность редактировать файлы в pull-зеркале
+ Возможность сделать зеркало с Azure
+ Восстановление пароля из настроек профиля
+ Дополнили вебхуки информацией о лейблах
+ Наследование ограничения на коммиты проектов в командах компании
+ Обновление времени изменения проекта после изменения ЗнС
+ Отображение картинок в вики из-за недостатка прав
+ Отображение команд/компаний в списке "с моим участием" до принятия приглашения
+ Отображение переключателя темы в Книге
+ Отображение подмодулей в подпапках
+ Отображение почты в настройках профиля, при ее смене
+ Отображение селектора выбора версии Книги без авторизации
+ Ошибку в работе владельцев кода (codeowners)
+ Ошибку отображения стандартной и рабочей ветки у зеркала
+ Ошибку пагинации в API методах
+ Ошибку при выборе ветки в ЗнС из форка в исходный проект
+ Ошибку при добавлении пользователей в список участников
+ Ошибку при отключении защиты ветки/тега
+ Ошибку регистрации по почте при запрете на регистрацию (Self-Hosted)
+ Ошибку с доступом к проекту без авторизации
+ Ошибку с подключением 2FA в настройках профиля
+ Ошибку сохранения cookie для сессии при закрытии браузера
+ Подсветку кода популярных языков программирования при просмотре файлов
+ Проверку контрольной строки при создании push-зеркала
+ Проверку на использование 2FA при работе по http/https
+ Сообщение при создании команд/компаний с занятым алиасом
+ Уведомления пользователю, когда он подписан на все обновления
+ Верстку стадий конвейеров (CI/CD)
+ Обработку ошибки конвейеров без задач (CI/CD)
+ Ошибку в работе параметра needs (CI/CD)
+ Ошибку при импорте переменных (CI/CD)
+ Парсинг параметра needs (CI/CD)
+ Указание стадий по умолчанию для задач (CI/CD)

### API:

+ Метод создания тега, ссылаясь на коммит
+ Метод отображения текущих задач агента
+ Методы для работы с Агентами GitFlic
+ Методы для создания форка проекта
+ Метод получения последнего релиза
+ Метод передачи команды
+ Методы для работы с пользователями в проекте/команде/компании
+ Методы для работы с коммитами
+ Методы для сравнения коммитов и тегов
+ Метод для удаления проекта
+ Исправили отображение списка файлов в методе на получение списка релизов