## 2.16.0 Что нового:

#### Добавили:

+ Обновленный внешний вид
+ Темная и светлая темы
+ Относительные пути для изображений в Readme
+ Управление навигацией вкладок в проекте
+ Возможность быстрого выбора текущего пользователя ответственным в запросах на слияние и проблемах
+ Удаление администратором проекта комментариев от всех пользователей
+ Возможность отправить тестовый вебхук по URL

#### Исправили: 

+ Работу запросов на слияние из форка
+ Копирование из блока с кодом по кнопке "Скопировать"
+ Заменили название ветки "Дефолтная"  на "Стандартная"
+ Переходы в сервис из книги
+ Метод API с получением информации о файле из коммита
+ Пагинатор на страницах сервиса
+ Заменили иконку для редактирования комментариев
+ Отображение тени на изображениях в Markdown
+ Страницу с добавлением ssh-ключей
+ Отображение изображений в приватных книгах проекта
+ Отображение сообщения о синхронизации проекта-форка
+ Убрали вкладку вики из проекта-вики
+ Ошибку при указании директории с репозиториями (self-hosted)
+ Логику работы кнопки создания конвейера (self-hosted)

---

## 2.16.1 Что нового:

#### Исправили:

+ Работу force push по умолчанию и в защищенных ветках
+ Стили некоторых всплывающих окон
+ Верстку на страницах в мобильной версии, книге, проектах
+ Ошибку с удалением владельца команды
+ Убрали кнопку жалобы со страницы активного профиля

---

## 2.16.2 Что нового:

#### Добавили:

+ Переключатель темной темы

#### Исправили:

+ Привели к общим наборам события вебхуков и Telegram
+ Содержание некоторых шаблонов сообщений
+ Ошибки в верстке на страницах сервиса
+ Ошибку в подсчете пользователей в компаниях и командах, при добавлении участников в проект
+ Фильтр файлов в запросах на слияние
+ Ошибку создание релиза без указания тега
